# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import timesheet


def register():
    Pool.register(
        timesheet.Timesheet,
        module='timesheet_interval', type_='model')
    Pool.register(
        timesheet.TeamTimesheet,
        timesheet.TeamTimesheetWork,
        timesheet.TimesheetTeamWorkCompanyEmployee,
        timesheet.EditWorkTimesheetStart,
        module='timesheet_interval', type_='model',
        depends=['team_timesheet'])
    Pool.register(
        timesheet.EditWorkTimesheets,
        module='timesheet_interval', type_='wizard',
        depends=['team_timesheet'])
    Pool.register(
        timesheet.TeamTimesheetProductionDaily,
        timesheet.TimesheetTeamWorkCompanyEmployeeProductionDaily,
        timesheet.TeamTimesheetWorkProductionDaily,
        timesheet.TimesheetProductionDaily,
        module='timesheet_interval', type_='model',
        depends=['production_daily_end_time', 'team_timesheet'])
