====
Timesheet Abosolute hours manage
====

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard

Install timesheet_interval::

    >>> config = activate_modules('timesheet_interval')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create timesheet lines::

    >>> Employee = Model.get('company.employee')
    >>> Party = Model.get('party.party')
    >>> Work = Model.get('timesheet.work')
    >>> Line = Model.get('timesheet.line')
    >>> party = Party(name='Empleado 1')
    >>> party.save()
    >>> employee = Employee(company=company, party=party)
    >>> employee.save()
    >>> work = Work(name='Work 1')
    >>> work.save()
    >>> today = datetime.date.today()
    >>> yesterday = datetime.datetime.combine(today, datetime.time.min) + datetime.timedelta(days=-1)
    >>> Line(employee=employee, date=today, work=work, start_time=yesterday, end_time=yesterday).save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "End time" in "Timesheet Line" is not valid according to its domain. - 
    >>> tomorow = datetime.datetime.combine(today, datetime.time.min) + datetime.timedelta(days=1)
    >>> Line(employee=employee, date=today, work=work, start_time=tomorow, end_time=yesterday).save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "End time" in "Timesheet Line" is not valid according to its domain. - 
    >>> now = datetime.datetime.combine(today, datetime.time.min) + datetime.timedelta(hours=10)
    >>> Line(employee=employee, date=today, work=work, start_time=now).save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.RequiredValidationError: A value is required for field "End time" in "Timesheet Line". - 
    >>> Line(employee=employee, date=today, work=work, start_time=now, end_time=now + datetime.timedelta(hours=-1)).save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "End time" in "Timesheet Line" is not valid according to its domain. - 
    >>> line = Line(employee=employee, date=today, work=work, start_time=now, end_time=now + datetime.timedelta(hours=+2))
    >>> line.save()
    >>> line.duration == datetime.timedelta(hours=2)
    True
    >>> line.date = today + datetime.timedelta(days=1)
    >>> line.duration == datetime.timedelta(hours=2)
    True
    >>> line.start_time == line.end_time + datetime.timedelta(hours=-2)
    True
    >>> line.save()


