============================================
Timesheet interval production daily scenario
============================================

Imports::

    >>> from datetime import datetime, time, timedelta
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools import create_team, get_team
    >>> from proteus import Model, Wizard

Install timesheet_interval::

    >>> config = activate_modules(['timesheet_interval', 'production_daily_end_time', 'team_timesheet'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create team::

    >>> _ = create_team()
    >>> team = get_team()

Create work::

    >>> Work = Model.get('timesheet.work')
    >>> work1 = Work(name='Work1')
    >>> work1.save()

Add daily end time in production configuration::

    >>> Configuration = Model.get('production.configuration')
    >>> config = Configuration()
    >>> config.daily_end_time = time(8)
    >>> config.save()

Check lower and upper date::

    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> today = datetime.today()
    >>> team_timesheet = TeamTimesheet(date=today, team=team)
    >>> team_timesheet.start_time = datetime(today.year, today.month, today.day , 8, 0)
    >>> team_timesheet.end_time = datetime(today.year, today.month, today.day, 18, 0)
    >>> tts_work = team_timesheet.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.start_time = datetime(today.year, today.month, today.day, 8, 0)
    >>> tts_work.end_time = datetime(today.year, today.month, today.day, 15, 0)
    >>> tts_work.duration = timedelta(hours=8)
    >>> team_timesheet.save()
    >>> team_timesheet.date_lower == datetime.combine(team_timesheet.date, time.min) \
    ...     + relativedelta(hours=8)
    True
    >>> team_timesheet.date_upper == datetime.combine(team_timesheet.date, time.min) \
    ...     + relativedelta(days=1, hours=8)
    True
    >>> tts_work.date_lower == datetime.combine(tts_work.tts_date, time.min) \
    ...     + relativedelta(hours=8)
    True
    >>> tts_work.date_lower == datetime.combine(tts_work.tts_date, time.min) \
    ...     + relativedelta(hours=8)
    True
    >>> tts_work.date_upper == datetime.combine(tts_work.tts_date, time.min) \
    ...     + relativedelta(days=1, hours=8)
    True
