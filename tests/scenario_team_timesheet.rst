=====================
Team Timesheet manage
====================

Imports::

    >>> import sys
    >>> import doctest
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools import create_team, get_team
    >>> from proteus import Model, Wizard
    >>> from datetime import timedelta, datetime
    >>> from dateutil.relativedelta import relativedelta


Install team_timesheet Module::

    >>> config = activate_modules(['timesheet_interval', 'team_timesheet'])


Get Models::

    >>> Group = Model.get('res.group')
    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> Timesheet = Model.get('timesheet.team.work-company.employee')
    >>> User = Model.get('res.user')
    >>> Work = Model.get('timesheet.work')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Reload the context::


    >>> config._context = User.get_preferences(True, config.context)
    >>> today = datetime.today()


Create team::

    >>> _ = create_team()
    >>> team = get_team()


Create work::

    >>> work1 = Work(name='Work1')
    >>> work1.save()


Create team timesheet::

    >>> team_timesheet = TeamTimesheet(date=today, team=team)
    >>> team_timesheet.start_time = datetime(today.year, today.month, today.day, 8, 0)
    >>> team_timesheet.end_time = datetime(today.year, today.month, today.day, 18, 0)
    >>> tts_work = team_timesheet.works.new()
    >>> tts_work.work=work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.start_time == team_timesheet.start_time
    True
    >>> tts_work.end_time == team_timesheet.end_time
    True
    >>> tts_work.start_time = datetime(today.year, today.month, today.day, 8, 0)
    >>> tts_work.end_time = datetime(today.year, today.month, today.day, 15, 0)
    >>> tts_work.duration.seconds // 3600
    7
    >>> tts_work.duration = timedelta(hours=8)
    >>> tts_work.end_time == datetime(today.year, today.month, today.day, 16, 0)
    True
    >>> team_timesheet.save()
    >>> tts_work.planned_duration.total_seconds() // 3600
    8.0


Edit work timesheets::

    >>> tts_work, = [w for w in team_timesheet.works]
    >>> edit_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.edit_timesheets', [tts_work])
    >>> edit_work_timesheets.form.start_time == tts_work.start_time
    True
    >>> edit_work_timesheets.form.end_time == tts_work.end_time
    True
    >>> timesheet, = edit_work_timesheets.form.timesheets
    >>> timesheet.start_time == tts_work.start_time
    True
    >>> timesheet.end_time == tts_work.end_time
    True
    >>> timesheet.duration = timedelta(hours=7)
    >>> timesheet.end_time != tts_work.end_time
    True

    >>> edit_work_timesheets.form.end_time = datetime(today.year, today.month, today.day, 14, 0)
    >>> edit_work_timesheets.form.duration.total_seconds() / 3600
    6.0
    >>> timesheet, = edit_work_timesheets.form.timesheets
    >>> timesheet.end_time == edit_work_timesheets.form.end_time
    True
    >>> timesheet.duration.total_seconds() / 3600
    6.0
    >>> edit_work_timesheets.execute('edit_timesheets')


Check tts_work::

    >>> tts_work.reload()
    >>> tts_work.end_time == datetime(today.year, today.month, today.day, 14, 0)
    True


Check start date and end time domains::

    >>> team_timesheet.start_time = datetime(2022, today.month, today.day, 8, 0)
    >>> team_timesheet.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Start time" in "Team Timesheet" is not valid according to its domain. - 

    >>> team_timesheet.start_time = datetime(today.year, today.month, today.day, 8, 0)
    >>> team_timesheet.save()

    >>> tts_work.start_time = datetime(today.year, today.month, today.day, 7, 0)
    >>> tts_work.end_time = team_timesheet.end_time
    >>> tts_work.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Start time" in "Timesheet Team Work - Company Employee" is not valid according to its domain. - 

    >>> tts_work.start_time = team_timesheet.start_time
    >>> tts_work.end_time = datetime(today.year, today.month, today.day, 20, 0)
    >>> tts_work.save()
    Traceback (most recent call last):
    ...
    trytond.model.modelstorage.DomainValidationError: The value for field "End time" in "Timesheet Team Work - Company Employee" is not valid according to its domain. - 

    >>> tts_work.start_time = team_timesheet.start_time
    >>> tts_work.end_time = team_timesheet.end_time
    >>> tts_work.save()


Check start date and end date domains in work with a tts without start and end times::

    >>> team_timesheet.start_time = None
    >>> team_timesheet.end_time = None

    >>> tts_work, = team_timesheet.works
    >>> tts_work.start_time = datetime(today.year -1, today.month, today.day, 0, 0)
    >>> tts_work.end_time = datetime(today.year, today.month, today.day, 11, 0)
    >>> team_timesheet.save()
    Traceback (most recent call last):
      ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Start time" in "Timesheet Team Work - Company Employee" is not valid according to its domain. - 

    >>> tts_work.start_time = datetime(today.year, today.month, today.day, 8, 0)
    >>> team_timesheet.save()


Check timesheet line::

    >>> team_timesheet.click('wait')
    >>> team_timesheet.click('confirm')
    >>> team_timesheet.click('do')

    >>> timesheet, = Timesheet.find([
    ...     ('team_timesheet', '=', team_timesheet),
    ...     ('tts_work', '=', tts_work)])
    >>> timesheet.start_time == tts_work.start_time
    True
    >>> timesheet.end_time == tts_work.end_time
    True
    >>> timesheet.duration == tts_work.duration
    True

    >>> timesheet_line, = team_timesheet.timesheets
    >>> timesheet_line.duration == timesheet.duration 
    True
    >>> timesheet_line.start_time == timesheet.start_time
    True
    >>> timesheet_line.end_time == timesheet.end_time
    True
