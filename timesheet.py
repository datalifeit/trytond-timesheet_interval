# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import dateutil
import pytz
from itertools import groupby
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from datetime import datetime, timedelta, time, date
from trytond.pyson import Eval, If, Bool, Or
from trytond.transaction import Transaction


class ExportMixin(object):

    @classmethod
    def export_data(cls, records, fields_names):
        pool = Pool()
        Company = pool.get('company.company')

        res = super().export_data(records, fields_names)

        company = Company(Transaction().context['company'])
        lzone = dateutil.tz.gettz(company.timezone
            ) if company.timezone else dateutil.tz.tzutc()
        szone = dateutil.tz.tzutc()

        for item in res:
            for _id_field, _field in enumerate(item):
                if isinstance(_field, datetime):
                    item[_id_field] = _field.replace(tzinfo=szone
                        ).astimezone(lzone).replace(tzinfo=None)

        return res


class IntervalMixin(ExportMixin):
    _interval_date_field = 'date'

    date_lower = fields.Function(fields.DateTime('Date lower'),
        'get_date_lower')
    date_upper = fields.Function(fields.DateTime('Date upper'),
        'get_date_upper')
    start_time = fields.DateTime('Start time', format="%H:%M",
        domain=[
            If(Bool(Eval('start_time', None)), [
                ('start_time', '>=', Eval('date_lower')),
                ('start_time', '<', Eval('date_upper'))],
                []
            )],
        states={'required': Bool(Eval('end_time', None))},
        depends=['start_time', 'date_lower', 'date_upper'])
    end_time = fields.DateTime('End time', format="%H:%M",
        domain=[
            If(Bool(Eval('end_time', None)),
                ('end_time', '>', Eval('start_time')),
                ()
            )],
        states={'required': Bool(Eval('start_time', None))},
        depends=['end_time', 'start_time'])

    @property
    def interval_date(self):
        return getattr(self, self._interval_date_field)

    def get_date_lower(self, name=None):
        date = datetime.combine(self.interval_date, time.min)
        return self.get_company_utc_date(date)

    def get_date_upper(self, name=None):
        date = datetime.combine(self.interval_date, time.min
            ) + timedelta(days=1)
        return self.get_company_utc_date(date)

    def get_company_utc_date(self, date):
        pool = Pool()
        Company = pool.get('company.company')
        company_id = Transaction().context.get('company', None)
        if company_id:
            company = Company(company_id)

            if company and company.timezone:
                timezone = pytz.timezone(company.timezone)
                local_date = timezone.localize(date, is_dst=None)
                date = local_date.astimezone(pytz.utc)

        return date


class IntervalDurationMixin(IntervalMixin):

    @fields.depends(methods=['_calc_hours_by_start_end_time'])
    def on_change_start_time(self):
        self._calc_hours_by_start_end_time()

    @fields.depends(methods=['_calc_hours_by_start_end_time'])
    def on_change_end_time(self):
        self._calc_hours_by_start_end_time()

    @fields.depends('start_time', 'end_time')
    def _calc_hours_by_start_end_time(self):
        if self.end_time and self.start_time:
            self.interval_duration = self.end_time - self.start_time
        elif self.start_time and self.interval_duration:
            self.end_time = self.start_time + self.interval_duration
        elif self.end_time and self.interval_duration:
            self.start_time = self.end_time + self.interval_duration
        else:
            self.interval_duration = timedelta(hours=0)

    @fields.depends('start_time', 'end_time', 'date_lower')
    def calc_start_end_date(self, new_date):
        self.date_lower = datetime.combine(new_date, time.min)
        self.date_upper = datetime.combine(
            new_date, time.min) + timedelta(days=1)
        if self.start_time:
            self.start_time = self.date_lower + (
                self.start_time - datetime.combine(
                    self.start_time, time.min))
            self.end_time = self.start_time + self.interval_duration


class Timesheet(IntervalDurationMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.line'

    @classmethod
    def get_readonly_fields(cls):
        fields = super().get_readonly_fields()
        fields.extend(['start_time', 'end_time'])

        return fields

    @property
    def interval_duration(self):
        return self.duration

    @interval_duration.setter
    def interval_duration(self, value):
        self.duration = value

    @fields.depends('date', methods=['calc_start_end_date'])
    def on_change_date(self):
        if self.date:
            self.calc_start_end_date(self.date)

    @fields.depends('duration', 'start_time', 'end_time')
    def on_change_duration(self):
        if not self.duration:
            return
        if self.start_time:
            self.end_time = self.start_time + self.duration
        elif self.end_time:
            self.start_time = self.end_time - self.duration

    @fields.depends('duration')
    def _calc_hours_by_start_end_time(self):
        super()._calc_hours_by_start_end_time()

    @fields.depends('duration')
    def calc_start_end_date(self, new_date):
        super().calc_start_end_date(new_date)


class TeamTimesheet(IntervalMixin, ExportMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        clause = Or(Bool(Eval('works', None)), Eval('state') != 'draft')
        for field_name in ['start_time', 'end_time']:
            field = getattr(cls, field_name)
            field.states['readonly'] = clause
            field.depends.extend(['works', 'state'])

        readonly = Bool(Eval('works', None))
        if cls.date.states.get('readonly'):
            cls.date.states['readonly'] |= readonly
        else:
            cls.date.states['readonly'] = readonly

        if 'works' not in cls.date.depends:
            cls.date.depends.append('works')

    @fields.depends('date')
    def on_change_date(self):
        if self.date:
            self.date_lower = self.get_date_lower()
            self.date_upper = self.get_date_upper()

    @classmethod
    def _create_tts_work_employee(cls, tts_work, tts_employee):
        twe = super()._create_tts_work_employee(tts_work, tts_employee)

        twe.start_time = tts_work.start_time
        twe.end_time = tts_work.end_time
        return twe


class TeamTimesheetWork(IntervalDurationMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._interval_date_field = 'tts_date'
        cls.start_time.states['readonly'] = (Eval('tts_state') != 'draft')
        cls.start_time.depends.append('tts_state')
        cls.end_time.states['readonly'] = (Eval('tts_state') != 'draft')
        cls.end_time.depends.append('tts_state')
        cls.start_time.domain.append(
            If(Bool(
                Eval('_parent_team_timesheet', {}).get('start_time', None)),
                ('start_time', '>=', Eval('_parent_team_timesheet', {}
                    ).get('start_time')),
                ()))
        cls.end_time.domain.append(
            If(Bool(
                Eval('_parent_team_timesheet', {}).get('end_time', None)),
                ('end_time', '<=', Eval('_parent_team_timesheet', {}
                    ).get('end_time')),
                ()))

    @property
    def interval_duration(self):
        return self.duration

    @interval_duration.setter
    def interval_duration(self, value):
        self.duration = value

    @fields.depends('duration')
    def _calc_hours_by_start_end_time(self):
        super()._calc_hours_by_start_end_time()

    @fields.depends('duration')
    def calc_start_end_date(self, new_date):
        super().calc_start_end_date(new_date)

    @fields.depends('time_type', methods=['on_change_with_planned_duration'])
    def on_change_start_time(self):
        super().on_change_start_time()
        self.planned_duration = self.on_change_with_planned_duration()

    @fields.depends('time_type', methods=['on_change_with_planned_duration'])
    def on_change_end_time(self):
        super().on_change_end_time()
        self.planned_duration = self.on_change_with_planned_duration()

    @fields.depends('duration', 'start_time', 'end_time')
    def on_change_duration(self):
        duration = self.duration or timedelta(0)
        if self.start_time:
            self.end_time = self.start_time + duration
        elif self.end_time:
            self.start_time = self.end_time - duration

    @fields.depends('team_timesheet', '_parent_team_timesheet.date',
        '_parent_team_timesheet.start_time', '_parent_team_timesheet.end_time',
        methods=['on_change_with_tts_date'])
    def on_change_team_timesheet(self):
        super(TeamTimesheetWork, self).on_change_team_timesheet()
        if self.team_timesheet and self.team_timesheet.date:
            self.tts_date = self.on_change_with_tts_date()
            self.date_lower = self.get_date_lower()
            self.date_upper = self.get_date_upper()
            self.start_time = self.team_timesheet.start_time
            self.end_time = self.team_timesheet.end_time
            if self.end_time and self.start_time:
                self.duration = self.end_time - self.start_time
            else:
                self.duration = timedelta(0)

    @classmethod
    def validate(cls, records):
        for r in records:
            r.calc_start_end_date(r.team_timesheet.date)
        super(TeamTimesheetWork, cls).validate(records)

    def _update_date(self, date):
        super()._update_date(date)
        self.calc_start_end_date(date)
        for line in self.timesheets:
            line.calc_start_end_date(date)

    @classmethod
    def write(cls, *args):
        Timesheet = Pool().get('timesheet.team.timesheet')

        actions = iter(args)
        for tts_works, values in zip(actions, actions):
            tts_works = sorted(tts_works, key=lambda x: x.team_timesheet)
            for team_timesheet, tts_works in groupby(tts_works,
                    key=lambda x: x.team_timesheet):
                kwargs = {}
                if 'start_time' in values:
                    kwargs['start_time'] = values['start_time']
                if 'end_time' in values:
                    kwargs['end_time'] = values['end_time']

                if kwargs:
                    Timesheet.edit_tts_work_employee(list(tts_works),
                        team_timesheet.employees, **kwargs)

        super().write(*args)


class TimesheetTeamWorkCompanyEmployee(IntervalDurationMixin,
        metaclass=PoolMeta):
    __name__ = 'timesheet.team.work-company.employee'

    tts_start_time = fields.Function(
        fields.DateTime('TTS start time'),
        'on_change_with_tts_start_time')
    tts_end_time = fields.Function(
        fields.DateTime('TTS end time'),
        'on_change_with_tts_end_time')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._interval_date_field = 'tts_date'

        cls.start_time.domain.append(
            If(Bool(Eval('tts_start_time', None)),
                ('start_time', '>=', Eval('tts_start_time')),
                ()))
        cls.start_time.depends.append('tts_start_time')

        cls.end_time.domain.append(
            If(Bool(Eval('tts_end_time', None)),
                ('end_time', '<=', Eval('tts_end_time')),
                ()))
        cls.end_time.depends.append('tts_end_time')

    @property
    def interval_duration(self):
        return self.duration

    @interval_duration.setter
    def interval_duration(self, value):
        self.duration = value

    @fields.depends('duration', 'start_time', 'end_time')
    def on_change_duration(self):
        super().on_change_duration()
        if not self.duration:
            return
        if self.start_time:
            self.end_time = self.start_time + self.duration
        elif self.end_time:
            self.start_time = self.end_time - self.duration

    @fields.depends('team_timesheet', '_parent_team_timesheet.start_time')
    def on_change_with_tts_start_time(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.start_time

        return None

    @fields.depends('team_timesheet', '_parent_team_timesheet.end_time')
    def on_change_with_tts_end_time(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.end_time

        return None

    @fields.depends(methods=['on_change_duration'])
    def on_change_start_time(self):
        super().on_change_start_time()
        self.on_change_duration()

    @fields.depends(methods=['on_change_duration'])
    def on_change_end_time(self):
        super().on_change_end_time()
        self.on_change_duration()

    @classmethod
    def _create_timesheets(cls, record):
        line = super()._create_timesheets(record)
        line.start_time = record.start_time
        line.end_time = record.end_time
        return line


class EditWorkTimesheetStart(IntervalDurationMixin, metaclass=PoolMeta):
    __name__ = (
        'timesheet.team.timesheet-timesheet.work.edit_timesheets.start')

    tts_start_time = fields.DateTime('TTS start time', readonly=True)
    tts_end_time = fields.DateTime('TTS end time', readonly=True)
    tts_date = fields.Date('TTS Date', readonly=True)

    @classmethod
    def __setup__(cls):
        super().__setup__()

        cls._interval_date_field = 'tts_date'

        cls.start_time.domain.append(
            If(Bool(Eval('tts_start_time', None)),
                ('start_time', '>=', Eval('tts_start_time')),
                ()))
        cls.start_time.depends.append('tts_start_time')

        cls.end_time.domain.append(
            If(Bool(Eval('tts_end_time', None)),
                ('end_time', '<=', Eval('tts_end_time')),
                ()
            ))
        cls.end_time.depends.append('tts_end_time')

    @property
    def interval_duration(self):
        return self.duration

    @interval_duration.setter
    def interval_duration(self, value):
        self.duration = value

    @fields.depends('duration', 'start_time', 'end_time')
    def on_change_duration(self):
        if self.duration and self.start_time:
            self.end_time = self.start_time + self.duration
        elif self.duration and self.end_time:
            self.start_time = self.end_time - self.duration

        super().on_change_duration()

    @fields.depends('end_time', 'start_time', methods=['on_change_duration'])
    def on_change_start_time(self):
        super().on_change_start_time()
        self.duration = self.end_time - self.start_time
        self.on_change_duration()

    @fields.depends('end_time', 'start_time', methods=['on_change_duration'])
    def on_change_end_time(self):
        super().on_change_end_time()
        self.duration = self.end_time - self.start_time
        self.on_change_duration()

    @fields.depends('duration')
    def calc_start_end_date(self, new_date):
        super().calc_start_end_date(new_date)

    @fields.depends('duration')
    def _calc_hours_by_start_end_time(self):
        super()._calc_hours_by_start_end_time()


class EditWorkTimesheets(metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work.edit_timesheets'

    def default_start(self, fields):
        default_data = super().default_start(fields)

        tts_date = self.record.team_timesheet.date
        data = dict(
            tts_start_time=self.record.team_timesheet.start_time,
            tts_end_time=self.record.team_timesheet.end_time,
            tts_date=tts_date,
            start_time=self.record.start_time,
            end_time=self.record.end_time,
            date_lower=datetime.combine(tts_date, time.min),
            date_upper=datetime.combine(tts_date, time.min) + timedelta(days=1)
        )
        default_data.update(data)

        return default_data

    def transition_edit_timesheets(self):
        if self.record.start_time != self.start.start_time:
            self.record.start_time = self.start.start_time
            self.record.save()

        if self.record.end_time != self.start.end_time:
            self.record.end_time = self.start.end_time
            self.record.save()

        return super().transition_edit_timesheets()


class ProductionDailyMixin(object):

    def get_date_lower(self, name=None):
        Configuration = Pool().get('production.configuration')

        _date = super().get_date_lower(name)

        config = Configuration(1)
        daily_end_time = None
        if config.daily_end_time:
            daily_end_time = datetime.combine(
                date.min, config.daily_end_time) - datetime.min

        return _date + daily_end_time if daily_end_time else _date

    def get_date_upper(self, name=None):
        Configuration = Pool().get('production.configuration')

        _date = super().get_date_upper(name)

        config = Configuration(1)
        daily_end_time = None
        if config.daily_end_time:
            daily_end_time = datetime.combine(
                date.min, config.daily_end_time) - datetime.min

        return _date + daily_end_time if daily_end_time else _date


class TeamTimesheetProductionDaily(ProductionDailyMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet'


class EditWorkTimesheetStartProductionDaily(ProductionDailyMixin,
        metaclass=PoolMeta):
    __name__ = (
        'timesheet.team.timesheet-timesheet.work.edit_timesheets.start')


class TimesheetTeamWorkCompanyEmployeeProductionDaily(ProductionDailyMixin,
        metaclass=PoolMeta):
    __name__ = 'timesheet.team.work-company.employee'


class TeamTimesheetWorkProductionDaily(ProductionDailyMixin,
        metaclass=PoolMeta):
    __name__ = 'timesheet.team.timesheet-timesheet.work'


class TimesheetProductionDaily(ProductionDailyMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.line'
